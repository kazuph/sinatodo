$(function() {

  $('button.create').on('click', function() {
    var title = $('.title').val();
    if ($('.title').val()) {
      $.ajax({
          url: '/create',
          type: 'POST',
          data: { title: title },
      }).success(function(data) {
        var tr = $('<tr></tr>'), td = [];
        td.push('<td class="title">' + data.title + '</td>');
        td.push('<td class="created_on">' + data.created_on + '</td>');
        td.push('<td class="updated_on">' + data.updated_on + '</td>');
        td.push('<td><button class="btn btn-danger delete" value="' + data.id + '">削除</button></td>');
        tr.append(td.join((''))).appendTo($('table.todoList tbody')).hide().fadeIn('slow');
        $('.title').val('');
      });
    }
  });

  $('button.delete').live('click', function() {
    var button = $(this);
    $.ajax({
      url: '/delete',
      type: 'PUT',
      data: { id: button.val() },
    }).success(function(data) {
      button.parents('tr').fadeOut('slow');
    });
  })


  $('td.title').live('click', function () {
    if (!$(this).hasClass('on')) {
      $(this).addClass('on');
      $(this).html('<input type="text" value="' + $(this).text() + '" />');
      $('td > input').focus().blur(function () {
        var inputVal = $(this).val();
        if (inputVal === '') {
            inputVal = this.defaultValue;
        };

        var title      = $(this);
        var updated_on = $(this).parents('tr').find('td.updated_on');
        $.ajax({
          url: '/edit',
          type: 'PUT',
          data: {
            id:    $(this).parents('tr').find('button').val(),
            title: inputVal
          },
        }).success(function(data) {
          title.parent().removeClass('on').text(inputVal);
          updated_on.html(data.updated_on).fadeIn('slow');
        });

      });
    };
  });

});
