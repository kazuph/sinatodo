# -*- encoding: UTF-8 -*-

require 'sinatra'
require 'erb'
require 'active_record'
require 'json'
require 'logger'

set :views, File.dirname(__FILE__) + '/views'

ActiveRecord::Base.establish_connection(
  :adapter  => 'mysql2',
  :host     => 'localhost',
  :username => 'root',
  :password => '',
  :database => 'todo'
)

ActiveRecord::Base.logger = Logger.new(STDOUT)

class Todo < ActiveRecord::Base
  set_table_name :todo
end

get '/' do
  @todo = Todo.where(:active => 1);
  erb :index
end

post '/create' do
  todo = Todo.new(
    :title      => params[:title],
  )
  todo.save
  content_type :json, :charset => 'utf-8'
  {
    :id         => todo.id,
    :title      => todo.title,
    :created_on => todo.created_on.strftime("%Y年%m月%d日 %H時%M分"),
    :updated_on => todo.updated_on.strftime("%Y年%m月%d日 %H時%M分"),
  }.to_json
end

put '/edit' do
  todo = Todo.update(params[:id], :title => params[:title])

  content_type :json, :charse => 'urf-8'
  { :updated_on => todo.updated_on.strftime("%Y年%m月%d日 %H時%M分") }.to_json
end

put '/delete' do
  Todo.update(params[:id], :active => 0)
end
